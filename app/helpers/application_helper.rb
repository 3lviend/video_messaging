module ApplicationHelper

	def content_type(video)
		vid = video.instance.video_file_name unless video.instance.blank?

		content_type = if vid.blank?
			'webm'
		else
			vid.split('.').last
		end
	end
end

class Message < ActiveRecord::Base

belongs_to :category

has_attached_file :video, :path => ":rails_root/public:url",
						  :url => "/system/:class/:attachment/:id/:style/:basename.:extension",
						  :default_url => "/videos/original/example.webm"

validates_attachment_content_type :video, :content_type => ['application/x-matroska',"audio/mp4", "video/mp4", "video/webm", 'application/octet-stream']

PARENTS = [1,2,3,4]

# def message_not_blank_when_submitted
#   if body.empty? && !video?
#     errors.add(:body, 'must not be blank upon submit')
#   end
# end

end

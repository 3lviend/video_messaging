class CategoriesController < ApplicationController

	def index
		@categories = Category.all
		@category = params["id"].present? ? Category.find(params["id"]) : Category.first
		@messages = @category.messages.where(parent_id: nil)

	end

end

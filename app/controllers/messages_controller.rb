class MessagesController < ApplicationController
	
	skip_before_action :verify_authenticity_token, only: [:update_video]

	def index
		@parents = Message::PARENTS
		@message = Message.all.order("created_at DESC")

		@category_id = params["category_id"]
		if @category_id
			@message_parent = Category.find(@category_id).messages.where(parent_id:nil).last
			@messages = Category.find(@category_id).messages.where(parent_id:@message_parent.id).order("created_at DESC")
		end
	end

	def create_video
		@message = Message.new(message_params)
    if @message.save
    	render json: {success: true, message_id: @message.id}
    else
    	render json: {success: false}
    end
	end

	def update_video
		@message = Message.find(params["id"])
    if @message.update(message_params)
      redirect_to new_message_category_path(params["message"]["category_id"])
    else
      redirect_to root_url
    end
	end

	def message_params
	  params.require(:message).permit(:title, :description, :parent_id, :category_id, :video)
	end
end

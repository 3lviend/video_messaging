# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
  Category.create(name: 'Employes 1')
  Category.create(name: 'Employes 2')
  Category.create(name: 'Employes 3')
  Category.create(name: 'Employes 4')

  
   # => Message(id: integer, title: string, description: text, video_file_name: string, video_content_type: string, video_file_size: integer, video_updated_at: datetime, created_at: datetime, updated_at: datetime, parent_id: integer, category_id: integer) 
  Message.create(title: 'Title of Employes 1 part 1', description: "lorem ipsum dolor", category_id:1,)
  Message.create(title: 'Title of Employes 2', description: "lorem ipsum dolor", category_id:2)
  Message.create(title: 'Title of Employes 3', description: "lorem ipsum dolor", category_id:3)
  Message.create(title: 'Title of Employes 4', description: "lorem ipsum dolor", category_id:4)



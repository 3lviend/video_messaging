class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :title
      t.text :description
      t.attachment :video
      # t.string "video_file_name"
#       t.string "video_content_type"
#       t.integer "video_file_size"
#       t.datetime "video_updated_at"

      t.timestamps null: false
    end
  end
end
